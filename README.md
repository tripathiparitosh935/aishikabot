<h1 align="center">Hi 👋,Welcome I'am AISHIKA</h1>
<img src="https://user-images.githubusercontent.com/73097560/115834477-dbab4500-a447-11eb-908a-139a6edaec5c.gif">

## About me
I'am a Cognitive Behavioral Therapy Bot that will help to provide easy access to mental health care.

## In action

![logo](https://github.com/paritoshtripathi935/aishikabot/blob/main/Screenshot_2022-02-12_12-31-18.png)

## The problem Aishika-bot solves
The rise of A. I can be seen in many fields and it had shown great results.
so why not merge A. I with Cognitive Behavioral Therapy (CBT) and increase the access to mental healthcare especially In a country like India where we have very less regard for mental health also have very less mental health doctors.

The World Health Organization, in 2019, had estimated that 7.5% of Indians were affected by mental health disorders. This number would likely go up significantly because of the pandemic.
In a country with a deep mental health crisis, and with only above two mental health beds for every 1,00,000 population, mental health social workers, therapists and counsellors become a significant bridge in community health.

Using this kind of bot we can deploy these in mobile phones or websites and may be able to provide mental healthcare access in remote areas or to anybody who is ashamed of society to go to the doctor. Increasing awareness about mental health would be great and AISHIKA can help by making access more flexible.

## Challenges we ran into
One of the great challenges is to merge a chatbot with Cognitive Behavioral Therapy. So we need to learn a lot about this because what questions to ask is a great part of a CBT and especially which one to ask and then analyse the answer and return a well-matched response.

From this learning, we understood that key factors of a CBT session included:

1. “targeting emotions by changing thoughts and behaviours that are contributing to distressing emotions”
2. Creating a strong therapeutic relationship through empathy (validating the patient’s experience), genuineness (being authentic), - positive regard (respect)
3. Collaborative work of skill acquisition and homework to mould positive thought patterns
4. Teaching skills rather than just talking
5. Most importantly, the therapist must have collaborative, assertive, and nonjudgemental.

## how to use this

1. download ipynb notebook.
2. downlaod intents.json.
3. run the file and bot gui will be shown like above one.

